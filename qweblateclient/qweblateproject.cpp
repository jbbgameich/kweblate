// SPDX-FileCopyrightText: 2021 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: LGPL-3.0-only

#include "qweblateproject.h"

#include <QJsonObject>

#include <QUrl>

/*
{
    "name": "Hello",
    "slug": "hello",
    "url": "http://example.com/api/projects/hello/",
    "web": "https://weblate.org/",
    "web_url": "http://example.com/projects/hello/"
}

*/
class QWeblateProjectPrivate : public QSharedData {
public:
    QString name;
    QString slug;
    QUrl url;
    QUrl web;
    QUrl webUrl;
};

QWeblateProject::QWeblateProject(const QJsonObject &json, QWeblateClientPrivate *)
    : d(new QWeblateProjectPrivate())
{
    d->name = json.value(QStringLiteral("name")).toString();
    d->slug = json.value(QStringLiteral("slug")).toString();
    d->url = QUrl(json.value(QStringLiteral("url")).toString());
    d->web = QUrl(json.value(QStringLiteral("web")).toString());
    d->webUrl = QUrl(json.value(QStringLiteral("web_url")).toString());
}

const QString &QWeblateProject::name() const
{
    return d->name;
}

const QString &QWeblateProject::slug() const
{
    return d->slug;
}

const QUrl &QWeblateProject::url() const
{
    return d->url;
}

const QUrl &QWeblateProject::web() const
{
    return d->web;
}

const QUrl &QWeblateProject::webUrl() const
{
    return d->webUrl;
}

QWeblateProject::QWeblateProject(const QWeblateProject &other) = default;
QWeblateProject::~QWeblateProject() = default;
