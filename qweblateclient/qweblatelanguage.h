// SPDX-FileCopyrightText: 2021 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: LGPL-3.0-only

#pragma once

#include <QSharedDataPointer>

class QJsonObject;
class QWeblateClientPrivate;

class QWeblateLanguagePrivate;

class QWeblateLanguage
{
public:
    QWeblateLanguage(const QJsonObject &json, QWeblateClientPrivate *);
    ~QWeblateLanguage();
    QWeblateLanguage(const QWeblateLanguage &other);

    const QString &code() const;
    const QString &language() const;
    int total() const;
    int totalChars() const;
    int totalWords() const;
    int translated() const;
    int translatedChars() const;
    int translatedCharsPercent() const;
    int translatedPercent() const;
    int translatedWords() const;
    int translatedWordsPercent() const;

private:
    QSharedDataPointer<QWeblateLanguagePrivate> d;
};

