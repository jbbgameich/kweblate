// SPDX-FileCopyrightText: 2021 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: LGPL-3.0-only

#include "qweblategroup.h"

#include <QUrl>
#include <QJsonObject>

#include "qweblatejsonhelpers_p.h"

class QWeblateGroupPrivate : public QSharedData {
public:
    QString name;
    int projectSelection = -1;
    int languageSelection = -1;
    QUrl url;
    QVector<QString> roles;
    QVector<QString> languages;
    QVector<QString> projects;
    QString componentList;
    QVector<QString> components;
};

QWeblateGroup::QWeblateGroup(const QJsonObject &json, QWeblateClientPrivate *)
    : d(new QWeblateGroupPrivate())
{
    d->name = json.value(QStringLiteral("name")).toString();
    d->projectSelection = json.value(QStringLiteral("project_selection")).toInt();
    d->languageSelection = json.value(QStringLiteral("language_selection")).toInt();
    d->url = QUrl(json.value(QStringLiteral("url")).toString());
    d->roles = fromJsonArray<QString>(json.value(QStringLiteral("roles")).toArray());
    d->languages = fromJsonArray<QString>(json.value(QStringLiteral("languages")).toArray());
    d->projects = fromJsonArray<QString>(json.value(QStringLiteral("projects")).toArray());
    d->componentList = json.value(QStringLiteral("componentlist")).toString();
    d->components = fromJsonArray<QString>(json.value(QStringLiteral("components")).toArray());
}

const QString &QWeblateGroup::name() const
{
    return d->name;
}

int QWeblateGroup::projectSelection() const
{
    return d->projectSelection;
}

int QWeblateGroup::languageSelection() const
{
    return d->languageSelection;
}

const QUrl &QWeblateGroup::url() const
{
    return d->url;
}

const QVector<QString> &QWeblateGroup::roles() const
{
    return d->roles;
}

const QVector<QString> &QWeblateGroup::languages() const
{
    return d->languages;
}

const QVector<QString> &QWeblateGroup::projects() const
{
    return d->projects;
}

const QString &QWeblateGroup::componentList() const
{
    return d->componentList;
}

QWeblateGroup::QWeblateGroup(const QWeblateGroup &other) = default;
QWeblateGroup::~QWeblateGroup() = default;
