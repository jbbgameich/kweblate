#include "qweblatecomponent.h"

#include <QUrl>
#include <QJsonObject>

/*
      {
         "add_message" : "Added translation using Weblate ({{ language_name }})\n\n",
         "addon_message" : "Update translation files\n\nUpdated by \"{{ addon_name }}\" hook in Weblate.\n\nTranslation: {{ project_name }}/{{ component_name }}\nTranslate-URL: {{ url }}",
         "addons" : [],
         "agreement" : "",
         "allow_translation_propagation" : false,
         "auto_lock_error" : true,
         "branch" : "main",
         "changes_list_url" : "http://localhost/api/components/openboard/glossary/changes/",
         "check_flags" : "",
         "commit_message" : "Translated using Weblate ({{ language_name }})\n\nCurrently translated at {{ stats.translated_percent }}% ({{ stats.translated }} of {{ stats.all }} strings)\n\nTranslation: {{ project_name }}/{{ component_name }}\nTranslate-URL: {{ url }}",
         "commit_pending_age" : 24,
         "delete_message" : "Deleted translation using Weblate ({{ language_name }})\n\n",
         "edit_template" : true,
         "enable_suggestions" : true,
         "enforced_checks" : [],
         "file_format" : "tbx",
         "filemask" : "*.tbx",
         "git_export" : "http://localhost/git/openboard/glossary/",
         "glossary_color" : "silver",
         "id" : 4,
         "intermediate" : "",
         "is_glossary" : true,
         "language_code_style" : "",
         "language_regex" : "^[^.]+$",
         "license" : "",
         "license_url" : null,
         "links_url" : "http://localhost/api/components/openboard/glossary/links/",
         "lock_url" : "http://localhost/api/components/openboard/glossary/lock/",
         "manage_units" : true,
         "merge_message" : "Merge branch '{{ component_remote_branch }}' into Weblate.\n\n",
         "merge_style" : "rebase",
         "name" : "OpenBoard",
         "new_base" : "",
         "new_lang" : "none",
         "priority" : 100,
         "push" : "",
         "push_branch" : "",
         "push_on_commit" : true,
         "repo" : "local:",
         "report_source_bugs" : "",
         "repository_url" : "http://localhost/api/components/openboard/glossary/repository/",
         "repoweb" : "",
         "restricted" : false,
         "slug" : "glossary",
         "source_language" : {
            "aliases" : [
               "en_en",
               "base",
               "source",
               "enp",
               "eng"
            ],
            "code" : "en",
            "direction" : "ltr",
            "name" : "English",
            "plural" : {
               "formula" : "n != 1",
               "id" : 139,
               "number" : 2,
               "source" : 0,
               "type" : 1
            },
            "statistics_url" : "http://localhost/api/languages/en/statistics/",
            "url" : "http://localhost/api/languages/en/",
            "web_url" : "http://localhost/languages/en/"
         },
         "statistics_url" : "http://localhost/api/components/openboard/glossary/statistics/",
         "suggestion_autoaccept" : 0,
         "suggestion_voting" : false,
         "task_url" : null,
         "template" : "",
         "translations_url" : "http://localhost/api/components/openboard/glossary/translations/",
         "url" : "http://localhost/api/components/openboard/glossary/",
         "variant_regex" : "",
         "vcs" : "local",
         "web_url" : "http://localhost/projects/openboard/glossary/"
      }
*/

class QWeblateComponentPrivate : public QSharedData {
public:
    QString addMessage;
    QString addonMessage;
    // addons
    QString agreement;
    bool allowTranslationPropagation = false;
    bool autoLockError = false;
    QString branch;
    QString changesListUrl;
    QString checkFlags;
    QString commitMessage;
    bool commitPendingAge;
    QString deleteMessage;
    bool editTemplate = false;
    bool enableSuggestions = false;
    // enforcedChecks
    QString fileFormat;
    QString filemask;
    QString gitExport;
    QString glossaryColor;
    int id = -1;
    QString intermediate;
    bool isGlossary = false;
    QString languageCodeStyle;
    QString languageRegex;
    QString license;
    QUrl licenseUrl;
    QUrl linksUrl;
    QUrl lockUrl;
    bool manageUnits = false;
    QString mergeMessage;
    QString mergeStyle; // TODO consider making an enum
    QString name;
    QString newBase;
    QString newLang;
    int priority = 0;
    QString push;
    QString pushBranch;
    bool pushOnCommit = false;
    QString repo;
    QUrl reportSourceBugs;
    QUrl repositoryUrl;
    QUrl repoWeb;
    bool restricted = false;
    QString slug;
    // sourceLanguage
    QUrl statisticsUrl;
    int suggestionAutoaccept = 0;
    bool suggestionVoting = false;
    QUrl taskUrl;
    QString templateFile;
    QUrl translationsUrl;
    QUrl url;
    QString variantRegex;
    QString vcs;
    QUrl webUrl;
};

QWeblateComponent::QWeblateComponent(const QJsonObject &json, QWeblateClientPrivate *)
    : d(new QWeblateComponentPrivate())
{
    d->addMessage = json.value(QStringLiteral("addMessage")).toString();
    d->addonMessage = json.value(QStringLiteral("addonMessage")).toString();
    d->agreement = json.value(QStringLiteral("agreement")).toString();
    d->allowTranslationPropagation = json.value(QStringLiteral("allow_translation_propagation")).toBool();
    d->autoLockError = json.value(QStringLiteral("auto_lock_error")).toBool();
    d->branch = json.value(QStringLiteral("branch")).toString();
    d->changesListUrl = json.value(QStringLiteral("changes_list_url")).toString();
    d->checkFlags = json.value(QStringLiteral("check_flags")).toString();
    d->commitMessage = json.value(QStringLiteral("commit_message")).toString();
    d->commitPendingAge = json.value(QStringLiteral("commit_pending_age")).toInt();
    d->deleteMessage = json.value(QStringLiteral("delete_message")).toString();
    d->editTemplate = json.value(QStringLiteral("edit_template")).toBool();
    d->enableSuggestions = json.value(QStringLiteral("enable_suggestions")).toBool();
    d->fileFormat = json.value(QStringLiteral("file_format")).toString();
    d->filemask = json.value(QStringLiteral("filemask")).toString();
    d->gitExport = json.value(QStringLiteral("git_export")).toString();
    d->glossaryColor = json.value(QStringLiteral("glossary_color")).toString();
    d->id = json.value(QStringLiteral("id")).toInt();
    d->intermediate = json.value(QStringLiteral("intermediate")).toString();
    d->isGlossary = json.value(QStringLiteral("is_glossary")).toBool();
    d->languageCodeStyle = json.value(QStringLiteral("language_code_style")).toString();
    d->languageRegex = json.value(QStringLiteral("langage_regex")).toString();
    d->license = json.value(QStringLiteral("license")).toString();
    d->licenseUrl = QUrl(json.value(QStringLiteral("license_url")).toString());
    d->linksUrl = QUrl(json.value(QStringLiteral("links_url")).toString());
    d->lockUrl = QUrl(json.value(QStringLiteral("lock_url")).toString());
    d->manageUnits = json.value(QStringLiteral("manageUnits")).toBool();
    d->mergeMessage = json.value(QStringLiteral("merge_message")).toString();
    d->mergeStyle = json.value(QStringLiteral("merge_style")).toString();
    d->name = json.value(QStringLiteral("name")).toString();
    d->newBase = json.value(QStringLiteral("new_base")).toString();
    d->newLang = json.value(QStringLiteral("new_lang")).toString();
    d->priority = json.value(QStringLiteral("priority")).toInt();
    d->push = json.value(QStringLiteral("push")).toString();
    d->pushBranch = json.value(QStringLiteral("push_branc")).toString();
    d->pushOnCommit = json.value(QStringLiteral("push_on_commit")).toBool();
    d->repo = json.value(QStringLiteral("repo")).toString();
    d->reportSourceBugs = QUrl(json.value(QStringLiteral("report_source_bugs")).toString());
    d->repositoryUrl = QUrl(json.value(QStringLiteral("repositoryurl")).toString());
    d->repoWeb = QUrl(json.value(QStringLiteral("repoweb")).toString());
    d->restricted = json.value(QStringLiteral("restricted")).toBool();
    d->slug = json.value(QStringLiteral("slug")).toString();
    d->statisticsUrl = QUrl(json.value(QStringLiteral("statistics_url")).toString());
    d->suggestionAutoaccept = json.value(QStringLiteral("suggesstion_autoaccept")).toInt();
    d->suggestionVoting = json.value(QStringLiteral("suggestion_voting")).toBool();
    d->taskUrl = QUrl(json.value(QStringLiteral("task_url")).toString());
    d->templateFile = json.value(QStringLiteral("template")).toString();
    d->translationsUrl = QUrl(json.value(QStringLiteral("translations_url")).toString());
    d->url = QUrl(json.value(QStringLiteral("url")).toString());
    d->variantRegex = json.value(QStringLiteral("variant_regex")).toString();
    d->vcs = json.value(QStringLiteral("vcs")).toString();
    d->webUrl = json.value(QStringLiteral("vcs")).toString();
}

const QString &QWeblateComponent::addMessage() const
{
    return d->addMessage;
}

const QString &QWeblateComponent::addonMessage() const
{
    return d->addonMessage;
}

const QString &QWeblateComponent::agreement() const
{
    return d->agreement;
}

bool QWeblateComponent::allowTranslationPropagation() const
{
    return d->allowTranslationPropagation;
}

bool QWeblateComponent::autoLockError() const
{
    return d->autoLockError;
}

const QString &QWeblateComponent::changesListUrl() const
{
    return d->changesListUrl;
}

const QString &QWeblateComponent::checkFlags() const
{
    return d->checkFlags;
}

const QString &QWeblateComponent::commitMessage() const
{
    return d->commitMessage;
}

bool QWeblateComponent::commitPendingAge() const
{
    return d->commitPendingAge;
}

const QString &QWeblateComponent::deleteMessage() const
{
    return d->deleteMessage;
}

bool QWeblateComponent::editTemplate() const
{
    return d->editTemplate;
}

bool QWeblateComponent::enableSuggestions() const
{
    return d->enableSuggestions;
}

const QString &QWeblateComponent::fileFormat() const
{
    return d->fileFormat;
}

const QString &QWeblateComponent::filemask() const
{
    return d->filemask;
}

const QString &QWeblateComponent::gitExport() const
{
    return d->gitExport;
}

const QString &QWeblateComponent::glossaryColor() const
{
    return d->glossaryColor;
}

int QWeblateComponent::id() const
{
    return d->id;
}

const QString &QWeblateComponent::intermediate() const
{
    return d->intermediate;
}

bool QWeblateComponent::isGlossary() const
{
    return d->isGlossary;
}

const QString &QWeblateComponent::languageCodeStyle() const
{
    return d->languageCodeStyle;
}

const QString &QWeblateComponent::languageRegex() const
{
    return d->languageRegex;
}

const QString &QWeblateComponent::license() const
{
    return d->license;
}

const QUrl &QWeblateComponent::licenseUrl() const
{
    return d->licenseUrl;
}

const QUrl &QWeblateComponent::linksUrl() const
{
    return d->linksUrl;
}

bool QWeblateComponent::manageUnits() const
{
    return d->manageUnits;
}

const QString &QWeblateComponent::mergeMessage() const
{
    return d->mergeMessage;
}

const QString &QWeblateComponent::mergeStyle() const
{
    return d->mergeStyle;
}

const QString &QWeblateComponent::branch() const
{
    return d->branch;
}

const QString &QWeblateComponent::name() const
{
    return d->name;
}

const QString &QWeblateComponent::newBase() const
{
    return d->newBase;
}

const QString &QWeblateComponent::newLang() const
{
    return d->newLang;
}

int QWeblateComponent::priority() const
{
    return d->priority;
}

const QString &QWeblateComponent::push() const
{
    return d->push;
}

const QString &QWeblateComponent::pushBranch() const
{
    return d->pushBranch;
}

bool QWeblateComponent::pushOnCommit() const
{
    return d->pushOnCommit;
}

const QString &QWeblateComponent::repo() const
{
    return d->repo;
}

const QUrl &QWeblateComponent::reportSourceBugs() const
{
    return d->reportSourceBugs;
}

const QUrl &QWeblateComponent::repositoryUrl() const
{
    return d->repositoryUrl;
}

const QUrl &QWeblateComponent::repoWeb() const
{
    return d->repoWeb;
}

bool QWeblateComponent::restricted() const
{
    return d->restricted;
}

const QString &QWeblateComponent::slug() const
{
    return d->slug;
}

const QUrl &QWeblateComponent::statisticsUrl() const
{
    return d->statisticsUrl;
}

bool QWeblateComponent::suggestionAutoaccept() const
{
    return d->suggestionAutoaccept;
}

bool QWeblateComponent::suggestionVoting() const
{
    return d->suggestionVoting;
}

const QUrl &QWeblateComponent::taskUrl() const
{
    return d->taskUrl;
}

const QString &QWeblateComponent::templateFile() const
{
    return d->templateFile;
}

const QUrl &QWeblateComponent::translationsUrl() const
{
    return d->translationsUrl;
}

const QUrl &QWeblateComponent::url() const
{
    return d->url;
}

const QString &QWeblateComponent::variantRegex() const
{
    return d->variantRegex;
}

const QString &QWeblateComponent::vcs() const
{
    return d->vcs;
}

const QUrl &QWeblateComponent::webUrl() const
{
    return d->webUrl;
}

QWeblateComponent::~QWeblateComponent() = default;
QWeblateComponent::QWeblateComponent(const QWeblateComponent &other) = default;
