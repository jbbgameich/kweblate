// SPDX-FileCopyrightText: 2021 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: LGPL-3.0-only

#pragma once

#include <QSharedDataPointer>

class QJsonObject;
class QDateTime;
class QUrl;

class QWeblateClientPrivate;

class QWeblateTranslationPrivate;

class QWeblateTranslation
{
public:
    QWeblateTranslation(const QJsonObject &json, QWeblateClientPrivate *);
    ~QWeblateTranslation();
    QWeblateTranslation(const QWeblateTranslation &other);

    int failingChecks() const;
    double failingChecksPercent() const;
    int failingChecksWords() const;
    const QString &fileName() const;
    int fuzzy() const;
    double fuzzyPercent() const;
    int fuzzyWords() const;
    int haveComment() const;
    bool isTemplate() const;
    const QString &languageCode() const;
    const QString &lastAuthor() const;
    const QDateTime &lastChange() const;
    const QString &revision() const;
    const QUrl &shareUrl() const;
    int total() const;
    int totalWords() const;
    const QUrl &translateUrl() const;
    int translated() const;
    double translatedPercent() const;
    int translatedWords() const;
    const QUrl &url() const;
    const QUrl &webUrl() const;

private:
    QSharedDataPointer<QWeblateTranslationPrivate> d;
};

