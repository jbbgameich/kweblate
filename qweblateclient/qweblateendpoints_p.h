// SPDX-FileCopyrightText: 2021 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: LGPL-3.0-only

#pragma once

#include <QJsonObject>
#include <QSharedDataPointer>

class QWeblateEndpointsPrivate;

class QWeblateClientPrivate;

class QWeblateEndpoints
{
public:
    QWeblateEndpoints(const QJsonObject &json, QWeblateClientPrivate *);
    QWeblateEndpoints(const QWeblateEndpoints &other);
    ~QWeblateEndpoints();

    const QUrl &projects() const;
    const QUrl &components() const;
    const QUrl &translations() const;
    const QUrl &languagess() const;

private:
    QSharedDataPointer<QWeblateEndpointsPrivate> d;
};
