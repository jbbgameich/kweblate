#pragma once

#include <QSharedDataPointer>

class QJsonObject;
class QUrl;
class QWeblateClientPrivate;

class QWeblateComponentPrivate;

class QWeblateComponent
{
public:
    QWeblateComponent(const QJsonObject &json, QWeblateClientPrivate *);
    ~QWeblateComponent();
    QWeblateComponent(const QWeblateComponent &other);

    const QString &addMessage() const;
    const QString &addonMessage() const;
    const QString &agreement() const;
    bool allowTranslationPropagation() const;
    bool autoLockError() const;
    const QString &changesListUrl() const;
    const QString &checkFlags() const;
    const QString &commitMessage() const;
    bool commitPendingAge() const;
    const QString &deleteMessage() const;
    bool editTemplate() const;
    bool enableSuggestions() const;
    const QString &fileFormat() const;
    const QString &filemask() const;
    const QString &gitExport() const;
    const QString &glossaryColor() const;
    int id() const;
    const QString &intermediate() const;
    bool isGlossary() const;
    const QString &languageCodeStyle() const;
    const QString &languageRegex() const;
    const QString &license() const;
    const QUrl &licenseUrl() const;
    const QUrl &linksUrl() const;
    bool manageUnits() const;
    const QString &mergeMessage() const;
    const QString &mergeStyle() const;
    const QString &name() const;
    const QString &newBase() const;
    const QString &newLang() const;
    int priority() const;
    const QString &push() const;
    const QString &pushBranch() const;
    bool pushOnCommit() const;
    const QString &repo() const;
    const QUrl &reportSourceBugs() const;
    const QUrl &repositoryUrl() const;
    const QUrl &repoWeb() const;
    bool restricted() const;
    const QString &slug() const;
    const QUrl &statisticsUrl() const;
    bool suggestionAutoaccept() const;
    bool suggestionVoting() const;
    const QUrl &taskUrl() const;
    const QString &templateFile() const;
    const QUrl &translationsUrl() const;
    const QUrl &url() const;
    const QString &variantRegex() const;
    const QString &vcs() const;
    const QUrl &webUrl() const;
    const QString &branch() const;

private:
    QSharedDataPointer<QWeblateComponentPrivate> d;
};

