// SPDX-FileCopyrightText: 2021 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: LGPL-3.0-only

#pragma once


#include <QJsonObject>
#include <QUrl>

#include "qweblatejsonhelpers_p.h"
#include "qweblateclientprivate_p.h"

class QJsonObject;

class QWeblateResultsPrivate;

class QWeblateResultsBase {
public:
    QWeblateResultsBase(const QUrl &next, const QUrl &prev, QWeblateClientPrivate *client);
    ~QWeblateResultsBase();
    QWeblateResultsBase(const QWeblateResultsBase &other);

    const QUrl &nextUrl() const;
    const QUrl &prevUrl() const;

protected:
    QWeblateClientPrivate *clientPrivate() const;

    QSharedDataPointer<QWeblateResultsPrivate> d;
};

template <typename T>
class QWeblateResults : public QWeblateResultsBase
{
public:
    QWeblateResults(const QJsonObject &json, QWeblateClientPrivate *client)
        : QWeblateResultsBase(QUrl(json.value(QStringLiteral("next")).toString()),
                              QUrl(json.value(QStringLiteral("prev")).toString()),
                              client)
    {
        int count = json.value(QStringLiteral("count")).toInt();
        m_results = fromJsonArray<T>(json.value(QStringLiteral("results")).toArray(), client);

        Q_ASSERT(count >= m_results.size());
    }

    const QVector<T> &results() const {
        return m_results;
    }

    std::optional<QFuture<QWeblateResults<T>>> next() const {
        if (nextUrl().isValid()) {
            QWeblateClientPrivate *client = clientPrivate();
            if (client) {
                return client->getObject<QWeblateResults<T>>(nextUrl().path());
            }
        }

        return std::nullopt;
    }

    std::optional<QFuture<QWeblateResults<T>>> previous() const {
        if (nextUrl().isValid()) {
            QWeblateClientPrivate *client = clientPrivate();
            if (client) {
                return client->getObject<QWeblateResults<T>>(prevUrl().path());
            }
        }

        return std::nullopt;
    }

private:
    QVector<T> m_results;
};
