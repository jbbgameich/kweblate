// SPDX-FileCopyrightText: 2021 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: LGPL-3.0-only

#include "qweblatelanguage.h"

#include <QJsonObject>

class QWeblateLanguagePrivate : public QSharedData {
public:
    QString code;
    QString language;
    int total = 0;
    int totalChars = 0;
    int totalWords = 0;
    int translated = 0;
    int translatedChars = 0;
    int translatedCharsPercent = 0;
    int translatedPercent = 0;
    int translatedWords = 0;
    int translatedWordsPercent = 0;
};

QWeblateLanguage::QWeblateLanguage(const QJsonObject &json, QWeblateClientPrivate *)
    : d(new QWeblateLanguagePrivate())
{
    d->code = json.value(QStringLiteral("code")).toString();
    d->language = json.value(QStringLiteral("language")).toString();
    d->total = json.value(QStringLiteral("total")).toInt();
    d->totalChars = json.value(QStringLiteral("total_chars")).toInt();
    d->totalWords = json.value(QStringLiteral("total_words")).toInt();
    d->translated = json.value(QStringLiteral("translated")).toInt();
    d->translatedChars = json.value(QStringLiteral("translated_chars")).toInt();
    d->translatedCharsPercent = json.value(QStringLiteral("translated_chars_percent")).toInt();
    d->translatedPercent = json.value(QStringLiteral("translated_percent")).toInt();
    d->translatedWords = json.value(QStringLiteral("translated_words")).toInt();
    d->translatedWordsPercent = json.value(QStringLiteral("translated_words_percent")).toInt();
}

const QString &QWeblateLanguage::code() const
{
    return d->code;
}

const QString &QWeblateLanguage::language() const
{
    return d->language;
}

int QWeblateLanguage::total() const
{
    return d->total;
}

int QWeblateLanguage::totalChars() const
{
    return d->totalChars;
}

int QWeblateLanguage::totalWords() const
{
    return d->totalWords;
}

int QWeblateLanguage::translated() const
{
    return d->translated;
}

int QWeblateLanguage::translatedChars() const
{
    return d->translatedChars;
}

int QWeblateLanguage::translatedCharsPercent() const
{
    return d->translatedCharsPercent;
}

int QWeblateLanguage::translatedPercent() const
{
    return d->translatedPercent;
}

int QWeblateLanguage::translatedWords() const
{
    return d->translatedWords;
}

int QWeblateLanguage::translatedWordsPercent() const
{
    return d->translatedWordsPercent;
}

QWeblateLanguage::QWeblateLanguage(const QWeblateLanguage &other) = default;
QWeblateLanguage::~QWeblateLanguage() = default;
