// SPDX-FileCopyrightText: 2021 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: LGPL-3.0-only

#pragma once

#include <QSharedDataPointer>

class QJsonObject;
class QUrl;

class QWeblateClientPrivate;

class QWeblateGroupPrivate;

class QWeblateGroup
{
public:
    QWeblateGroup(const QJsonObject &json, QWeblateClientPrivate *);
    ~QWeblateGroup();
    QWeblateGroup(const QWeblateGroup &other);

    const QString &name() const;
    int projectSelection() const;
    int languageSelection() const;
    const QUrl &url() const;
    const QVector<QString> &roles() const;
    const QVector<QString> &languages() const;
    const QVector<QString> &projects() const;
    const QString &componentList() const;

private:
    QSharedDataPointer<QWeblateGroupPrivate> d;
};

