// SPDX-FileCopyrightText: 2021 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: LGPL-3.0-only

#pragma once

#include <QSharedDataPointer>

class QJsonObject;
class QUrl;

class QWeblateClientPrivate;

class QWeblateProjectPrivate;

class QWeblateProject
{
public:
    QWeblateProject(const QJsonObject &json, QWeblateClientPrivate *);
    ~QWeblateProject();
    QWeblateProject(const QWeblateProject &other);

    const QString &name() const;
    const QString &slug() const;
    const QUrl &url() const;
    const QUrl &web() const;
    const QUrl &webUrl() const;

private:
    QSharedDataPointer<QWeblateProjectPrivate> d;
};

