// SPDX-FileCopyrightText: 2021 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: LGPL-3.0-only

#pragma once

#include <QSharedDataPointer>

class QJsonObject;
class QDateTime;
class QUrl;

class QWeblateClientPrivate;

class QWeblateUserPrivate;

class QWeblateUser
{
public:
    QWeblateUser(const QJsonObject &object, QWeblateClientPrivate *);
    ~QWeblateUser();
    QWeblateUser(const QWeblateUser &other);

    const QString &email() const;
    void setEmail(const QString &email);

    const QString &fullName() const;
    void setFullName(const QString &fullName);

    const QString &username() const;
    void setUsername(const QString &username);

    const QVector<QString> &groups() const;
    void setGroups(const QVector<QString> &groups);

    bool isSuperuser() const;
    void setIsSuperuser(bool isSuperuser);

    bool isActive() const;
    void setIsActive(bool isActive);

    const QDateTime &dateJoined() const;
    const QUrl &url() const;
    const QUrl &statisticsUrl() const;

private:
    QSharedDataPointer<QWeblateUserPrivate> d;
};

