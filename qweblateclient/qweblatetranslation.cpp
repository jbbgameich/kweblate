// SPDX-FileCopyrightText: 2021 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: LGPL-3.0-only

#include "qweblatetranslation.h"

#include <QJsonObject>

/*
{
    "component": {
        "branch": "main",
        "file_format": "po",
        "filemask": "po/*.po",
        "git_export": "",
        "license": "",
        "license_url": "",
        "name": "Weblate",
        "new_base": "",
        "project": {
            "name": "Hello",
            "slug": "hello",
            "source_language": {
                "code": "en",
                "direction": "ltr",
                "name": "English",
                "url": "http://example.com/api/languages/en/",
                "web_url": "http://example.com/languages/en/"
            },
            "url": "http://example.com/api/projects/hello/",
            "web": "https://weblate.org/",
            "web_url": "http://example.com/projects/hello/"
        },
        "repo": "file:///home/nijel/work/weblate-hello",
        "slug": "weblate",
        "template": "",
        "url": "http://example.com/api/components/hello/weblate/",
        "vcs": "git",
        "web_url": "http://example.com/projects/hello/weblate/"
    },
    "failing_checks": 3,
    "failing_checks_percent": 75.0,
    "failing_checks_words": 11,
    "filename": "po/cs.po",
    "fuzzy": 0,
    "fuzzy_percent": 0.0,
    "fuzzy_words": 0,
    "have_comment": 0,
    "have_suggestion": 0,
    "is_template": false,
    "language": {
        "code": "cs",
        "direction": "ltr",
        "name": "Czech",
        "url": "http://example.com/api/languages/cs/",
        "web_url": "http://example.com/languages/cs/"
    },
    "language_code": "cs",
    "last_author": "Weblate Admin",
    "last_change": "2016-03-07T10:20:05.499",
    "revision": "7ddfafe6daaf57fc8654cc852ea6be212b015792",
    "share_url": "http://example.com/engage/hello/cs/",
    "total": 4,
    "total_words": 15,
    "translate_url": "http://example.com/translate/hello/weblate/cs/",
    "translated": 4,
    "translated_percent": 100.0,
    "translated_words": 15,
    "url": "http://example.com/api/translations/hello/weblate/cs/",
    "web_url": "http://example.com/projects/hello/weblate/cs/"
}
*/
class QWeblateTranslationPrivate : public QSharedData {
public:
    // component
    int failingChecks;
    double failingChecksPercent;
    int failingChecksWords;
    QString fileName;
    int fuzzy;
    double fuzzyPercent;
    int fuzzyWords;
    int haveComment;
    int haveSuggestion;
    bool isTemplate;
    // language
    QString languageCode;
    QString lastAuthor;
    QDateTime lastChange;
    QString revision;
    QUrl shareUrl;
    int total;
    int totalWords;
    QUrl translateUrl;
    int translated;
    double translatedPercent;
    int translatedWords;
    QUrl url;
    QUrl webUrl;
};

QWeblateTranslation::QWeblateTranslation(const QJsonObject &json, QWeblateClientPrivate *)
    : d(new QWeblateTranslationPrivate())
{
    // TODO component
    d->failingChecks = json.value(QStringLiteral("failing_checks")).toInt();
    d->failingChecksPercent = json.value(QStringLiteral("failing_checks_percent")).toDouble();
    d->failingChecksWords = json.value(QStringLiteral("failing_checks_words")).toInt();
    d->fileName = json.value(QStringLiteral("filename")).toString();
    d->fuzzy = json.value(QStringLiteral("fuzzy")).toInt();
    d->fuzzyPercent = json.value(QStringLiteral("fuzzy_percent")).toDouble();
    d->haveComment = json.value(QStringLiteral("have_comment")).toInt();
    d->haveSuggestion = json.value(QStringLiteral("have_suggestion")).toInt();
    d->isTemplate = json.value(QStringLiteral("is_template")).toBool();
    // TODO language
    d->languageCode = json.value(QStringLiteral("language_code")).toString();
    d->lastAuthor = json.value(QStringLiteral("last_author")).toString();
    d->lastChange = QDateTime::fromString(json.value(QStringLiteral("last_change")).toString(), Qt::ISODate);
    d->revision = json.value(QStringLiteral("revision")).toString();
    d->shareUrl = QUrl(json.value(QStringLiteral("share_url")).toString());
    d->total = json.value(QStringLiteral("total")).toInt();
    d->totalWords = json.value(QStringLiteral("totalWords")).toInt();
    d->translateUrl = QUrl(json.value(QStringLiteral("translateUrl")).toString());
    d->translated = json.value(QStringLiteral("translated")).toInt();
    d->translatedPercent = json.value(QStringLiteral("translated_percent")).toDouble();
    d->translatedWords = json.value(QStringLiteral("translated_words")).toInt();
    d->url = QUrl(json.value(QStringLiteral("url")).toString());
    d->webUrl = QUrl(json.value(QStringLiteral("webUrl")).toString());
}

int QWeblateTranslation::failingChecks() const
{
    return d->failingChecks;
}

double QWeblateTranslation::failingChecksPercent() const
{
    return d->failingChecksPercent;
}

int QWeblateTranslation::failingChecksWords() const
{
    return d->failingChecksWords;
}

const QString &QWeblateTranslation::fileName() const
{
    return d->fileName;
}

int QWeblateTranslation::fuzzy() const
{
    return d->fuzzy;
}

double QWeblateTranslation::fuzzyPercent() const
{
    return d->fuzzyPercent;
}

int QWeblateTranslation::fuzzyWords() const
{
    return d->fuzzyWords;
}

int QWeblateTranslation::haveComment() const
{
    return d->haveComment;
}

bool QWeblateTranslation::isTemplate() const
{
    return d->isTemplate;
}

const QString &QWeblateTranslation::languageCode() const
{
    return d->languageCode;
}

const QString &QWeblateTranslation::lastAuthor() const
{
    return d->lastAuthor;
}

const QDateTime &QWeblateTranslation::lastChange() const
{
    return d->lastChange;
}

const QString &QWeblateTranslation::revision() const
{
    return d->revision;
}

const QUrl &QWeblateTranslation::shareUrl() const
{
    return d->shareUrl;
}

int QWeblateTranslation::total() const
{
    return d->total;
}

int QWeblateTranslation::totalWords() const
{
    return d->totalWords;
}

const QUrl &QWeblateTranslation::translateUrl() const
{
    return d->translateUrl;
}

int QWeblateTranslation::translated() const
{
    return d->translated;
}

double QWeblateTranslation::translatedPercent() const
{
    return d->translatedPercent;
}

int QWeblateTranslation::translatedWords() const
{
    return d->translatedWords;
}

const QUrl &QWeblateTranslation::url() const
{
    return d->url;
}

const QUrl &QWeblateTranslation::webUrl() const
{
    return d->webUrl;
}

QWeblateTranslation::~QWeblateTranslation() = default;
QWeblateTranslation::QWeblateTranslation(const QWeblateTranslation &other) = default;
