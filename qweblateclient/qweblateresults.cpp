// SPDX-FileCopyrightText: 2021 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: LGPL-3.0-only

#include "qweblateresults.h"

class QWeblateResultsPrivate : public QSharedData {
public:
    QUrl next;
    QUrl prev;
    QWeblateClientPrivate *client = nullptr;
};

QWeblateResultsBase::QWeblateResultsBase(const QUrl &next, const QUrl &prev, QWeblateClientPrivate *client)
    : d(new QWeblateResultsPrivate())
{
    d->next = next;
    d->prev = prev;
    d->client = client;
}

QWeblateResultsBase::QWeblateResultsBase(const QWeblateResultsBase &other) = default;
QWeblateResultsBase::~QWeblateResultsBase() = default;

const QUrl &QWeblateResultsBase::nextUrl() const
{
    return d->next;
}

const QUrl &QWeblateResultsBase::prevUrl() const
{
    return d->prev;
}

QWeblateClientPrivate *QWeblateResultsBase::clientPrivate() const
{
    return d->client;
}
