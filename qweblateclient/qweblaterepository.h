// SPDX-FileCopyrightText: 2021 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: LGPL-3.0-only

#pragma once

#include <QSharedDataPointer>

class QJsonObject;
class QWeblateClientPrivate;

class QWeblateRepositoryPrivate;

class QWeblateRepository
{
public:
    QWeblateRepository(const QJsonObject &json, QWeblateClientPrivate *);
    ~QWeblateRepository();
    QWeblateRepository(const QWeblateRepository &other);

    bool needsCommit() const;
    bool needsMerge() const;
    bool needsPush() const;

private:
    QSharedDataPointer<QWeblateRepositoryPrivate> d;
};

