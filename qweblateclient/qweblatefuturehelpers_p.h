// SPDX-FileCopyrightText: 2021 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: LGPL-3.0-only

#pragma once

#include <memory>

#include <QFutureWatcher>

template <typename T>
std::shared_ptr<QFutureWatcher<T>> makeSharedFutureWatcher() {
    return std::shared_ptr<QFutureWatcher<T>>(
                new QFutureWatcher<T>(),
                [](auto *watcher) {
        watcher->deleteLater();
    });
}

// From here on everything can probably be replaced with Qt6-native APIs

///
/// Execute a function or lambda when this QFuture finishes.
///
/// Pretty much the same as QFuture::then in Qt6
///
template <typename T, typename OP>
void finished_then(QFuture<T> &&future, OP fun) {
    auto watcher = makeSharedFutureWatcher<T>();
    QObject::connect(watcher.get(), &QFutureWatcher<T>::finished, [=] {
        fun(watcher->result());
    });
    watcher->setFuture(future);
}

///
/// Once the future finishes, execute a function on the result of the future,
/// and return the result as a future
///
template <typename T, typename O, typename OP>
QFuture<O> map_future(QFuture<T> &&future, OP fun) {
    auto futureInterface = std::make_shared<QFutureInterface<O>>();
    finished_then(std::move(future), [=](const auto &&result) -> void {
        futureInterface->reportResult(fun(std::move(result)));
        futureInterface->reportFinished();
    });

    return futureInterface->future();
}
