// SPDX-FileCopyrightText: 2021 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: LGPL-3.0-only

#include "qweblateuser.h"

#include <QDateTime>
#include <QUrl>
#include <QJsonObject>
#include <QJsonArray>

#include "qweblatejsonhelpers_p.h"

class QWeblateUserPrivate : public QSharedData {
public:
    QString email;
    QString fullName;
    QString username;
    QVector<QString> groups;
    bool isSuperuser = false;
    bool isActive = false;
    QDateTime dateJoined;
    QUrl url;
    QUrl statisticsUrl;
};

QWeblateUser::QWeblateUser(const QJsonObject &object, QWeblateClientPrivate *)
    : d(new QWeblateUserPrivate())
{
    d->email = object.value(QStringLiteral("email")).toString();
    d->fullName = object.value(QStringLiteral("full_name")).toString();
    d->username = object.value(QStringLiteral("username")).toString();
    d->groups = fromJsonArray<QString>(object.value(QStringLiteral("groups")).toArray());
    d->isSuperuser = object.value(QStringLiteral("is_superuser")).toBool();
    d->isActive = object.value(QStringLiteral("is_active")).toBool();
    d->dateJoined = QDateTime::fromString(object.value(QStringLiteral("date_joined")).toString(), Qt::ISODate);
    d->url = QUrl(object.value(QStringLiteral("url")).toString());
    d->statisticsUrl = QUrl(object.value(QStringLiteral("statistics_url")).toString());
}

const QString &QWeblateUser::email() const
{
    return d->email;
}

void QWeblateUser::setEmail(const QString &email)
{
    d->email = email;
}

const QString &QWeblateUser::fullName() const
{
    return d->fullName;
}

void QWeblateUser::setFullName(const QString &fullName)
{
    d->fullName = fullName;
}

const QString &QWeblateUser::username() const
{
    return d->username;
}

void QWeblateUser::setUsername(const QString &username)
{
    d->username = username;
}

const QVector<QString> &QWeblateUser::groups() const
{
    return d->groups;
}

void QWeblateUser::setGroups(const QVector<QString> &groups)
{
    d->groups = groups;
}

bool QWeblateUser::isSuperuser() const
{
    return d->isSuperuser;
}

void QWeblateUser::setIsSuperuser(bool isSuperuser)
{
    d->isSuperuser = isSuperuser;
}

bool QWeblateUser::isActive() const
{
    return d->isActive;
}

void QWeblateUser::setIsActive(bool isActive)
{
    d->isActive = isActive;
}

const QDateTime &QWeblateUser::dateJoined() const
{
    return d->dateJoined;
}

const QUrl &QWeblateUser::url() const
{
    return d->url;
}

const QUrl &QWeblateUser::statisticsUrl() const
{
    return d->statisticsUrl;
}

QWeblateUser::QWeblateUser(const QWeblateUser &other) = default;
QWeblateUser::~QWeblateUser() = default;
