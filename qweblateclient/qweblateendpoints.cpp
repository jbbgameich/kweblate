// SPDX-FileCopyrightText: 2021 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: LGPL-3.0-only

#include "qweblateendpoints_p.h"

#include <QUrl>
#include <QSharedData>

class QWeblateEndpointsPrivate : public QSharedData {
public:
    QUrl projects;
    QUrl components;
    QUrl translations;
    QUrl languages;
};

QWeblateEndpoints::QWeblateEndpoints(const QJsonObject &json, QWeblateClientPrivate *)
    : d(new QWeblateEndpointsPrivate())
{
    d->projects = json.value(QStringLiteral("projects")).toString();
    d->components = json.value(QStringLiteral("components")).toString();
    d->translations = json.value(QStringLiteral("translations")).toString();
    d->languages = json.value(QStringLiteral("languages")).toString();
}

const QUrl &QWeblateEndpoints::projects() const
{
    return d->projects;
}

const QUrl &QWeblateEndpoints::components() const
{
    return d->components;
}

const QUrl &QWeblateEndpoints::translations() const
{
    return d->translations;
}

const QUrl &QWeblateEndpoints::languagess() const
{
    return d->languages;
}

QWeblateEndpoints::QWeblateEndpoints(const QWeblateEndpoints &other) = default;
QWeblateEndpoints::~QWeblateEndpoints() = default;
