// SPDX-FileCopyrightText: 2021 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: LGPL-3.0-only

#pragma once

#include <QFuture>

#include <memory>

class QUrl;

class QWeblateEndpoints;
class QWeblateUser;
class QWeblateGroup;
class QWeblateTranslation;
class QWeblateProject;
class QWeblateRepository;
class QWeblateComponent;
class QWeblateLanguage;

template <typename T>
class QWeblateResults;

class QWeblateClientPrivate;

class QWeblateClient
{
public:
    QWeblateClient(const QUrl &instanceUrl, const QString &token);
    ~QWeblateClient();

    QFuture<QWeblateResults<QWeblateUser>> users();
    QFuture<QWeblateUser> user(const QString &name);

    QFuture<QWeblateResults<QWeblateGroup>> groups();
    QFuture<QWeblateGroup> group(int groupId);

    QFuture<QWeblateResults<QWeblateProject>> projects();
    QFuture<QWeblateProject> project(const QString &name);
    QFuture<QWeblateRepository> projectRepository(const QString &name);
    QFuture<QWeblateResults<QWeblateComponent>> projectComponents(const QString &name);
    QFuture<QVector<QWeblateLanguage>> projectLanguages(const QString &name);

    QFuture<QWeblateEndpoints> endpoints();

    QFuture<QWeblateResults<QWeblateTranslation>> translations();
    QFuture<QWeblateTranslation> translation(const QString &project, const QString &component, const QString &language);
    QFuture<QByteArray> translationsFetchFile(const QString &project, const QString &component, const QString &language);

private:
    std::unique_ptr<QWeblateClientPrivate> d;
};

