// SPDX-FileCopyrightText: 2021 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: LGPL-3.0-only

#pragma once

#include <algorithm>
#include <QVector>
#include <QJsonArray>
#include <QJsonObject>

template <typename T, typename ...Args>
QVector<T> fromJsonArray(const QJsonArray &input, Args ...args) {
    QVector<T> output;
    std::transform(input.begin(), input.end(), std::back_inserter(output), [&](const QJsonValue &value) {
        if constexpr(std::is_same_v<T, QString>) {
            return value.toString();
        } else if constexpr(std::is_same_v<T, bool>) {
            return value.toBool();
        } else if constexpr(std::is_same_v<T, double>) {
            return value.toDouble();
        } else if constexpr(std::is_same_v<T, int>) {
            return value.toInt();
        } else {
            return T(value.toObject(), args...);
        }
    });

    return output;
}
