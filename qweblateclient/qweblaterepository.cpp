// SPDX-FileCopyrightText: 2021 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: LGPL-3.0-only

#include "qweblaterepository.h"

#include <QJsonObject>

class QWeblateRepositoryPrivate : public QSharedData {
public:
    bool needsCommit = false;
    bool needsMerge = false;
    bool needsPush = false;
};

QWeblateRepository::QWeblateRepository(const QJsonObject &json, QWeblateClientPrivate *)
    : d(new QWeblateRepositoryPrivate())
{
    d->needsCommit = json.value(QStringLiteral("needs_commit")).toBool();
    d->needsMerge = json.value(QStringLiteral("needs_merge")).toBool();
    d->needsPush = json.value(QStringLiteral("needs_push")).toBool();
}

QWeblateRepository::~QWeblateRepository() = default;
QWeblateRepository::QWeblateRepository(const QWeblateRepository &other) = default;

bool QWeblateRepository::needsCommit() const
{
    return d->needsCommit;
}

bool QWeblateRepository::needsMerge() const
{
    return d->needsMerge;
}

bool QWeblateRepository::needsPush() const
{
    return d->needsPush;
}
