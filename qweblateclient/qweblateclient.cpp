// SPDX-FileCopyrightText: 2021 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: LGPL-3.0-only

#include "qweblateclient.h"
#include "qweblateclientprivate_p.h"

#include "qweblateendpoints_p.h"
#include "qweblateuser.h"
#include "qweblatejsonhelpers_p.h"
#include "qweblateresults.h"
#include "qweblategroup.h"
#include "qweblatefuturehelpers_p.h"
#include "qweblatetranslation.h"
#include "qweblateproject.h"
#include "qweblaterepository.h"
#include "qweblatecomponent.h"
#include "qweblatelanguage.h"

QWeblateClient::QWeblateClient(const QUrl &instanceUrl, const QString &token)
    : d(std::make_unique<QWeblateClientPrivate>())
{
    d->instanceUrl = instanceUrl;
    d->token = token;
}

QWeblateClient::~QWeblateClient() = default;

QFuture<QWeblateResults<QWeblateUser>> QWeblateClient::users()
{
    return d->getObject<QWeblateResults<QWeblateUser>>(QStringLiteral("/api/users/"));
}

QFuture<QWeblateUser> QWeblateClient::user(const QString &name)
{
    return d->getObject<QWeblateUser>(u"/api/users/" % name % u'/');
}

QFuture<QWeblateResults<QWeblateGroup>> QWeblateClient::groups()
{
    return d->getObject<QWeblateResults<QWeblateGroup>>(QStringLiteral("/api/groups/"));
}

QFuture<QWeblateGroup> QWeblateClient::group(int groupId)
{
    return d->getObject<QWeblateGroup>(QStringLiteral(u"/api/groups/%0/").arg(groupId));
}

QFuture<QWeblateResults<QWeblateProject>> QWeblateClient::projects()
{
    return d->getObject<QWeblateResults<QWeblateProject>>(QStringLiteral("/api/projects/"));
}

QFuture<QWeblateProject> QWeblateClient::project(const QString &name)
{
    return d->getObject<QWeblateProject>(u"/api/projects/" % name % u'/');
}

QFuture<QWeblateRepository> QWeblateClient::projectRepository(const QString &name)
{
    return d->getObject<QWeblateRepository>(u"/api/projects/" % name % u"/repository/");
}

QFuture<QWeblateResults<QWeblateComponent> > QWeblateClient::projectComponents(const QString &name)
{
    return d->getObject<QWeblateResults<QWeblateComponent>>(u"/api/projects/" % name % u"/components/");
}

QFuture<QVector<QWeblateLanguage> > QWeblateClient::projectLanguages(const QString &name)
{
    return d->getArray<QWeblateLanguage>(u"/api/projects/" % name % u"/languages/");
}

QFuture<QWeblateEndpoints> QWeblateClient::endpoints()
{
    return d->getObject<QWeblateEndpoints>(QStringLiteral("/api/"));
}

QFuture<QWeblateResults<QWeblateTranslation>> QWeblateClient::translations()
{
    return d->getObject<QWeblateResults<QWeblateTranslation>>(QStringLiteral("/api/translations/"));
}

QFuture<QWeblateTranslation> QWeblateClient::translation(const QString &project, const QString &component, const QString &language)
{
    return d->getObject<QWeblateTranslation>(u"/api/translations/" % project % u'/' % component % u'/' % language % u'/');
}

QFuture<QByteArray> QWeblateClient::translationsFetchFile(
        const QString &project,
        const QString &component,
        const QString &language)
{
    return d->authorizedGetRequest(u"/api/translations/" % project % u'/' % component % u'/' % language % u"/file/" );
}
