// SPDX-FileCopyrightText: 2021 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: LGPL-3.0-only

#pragma once

#include <QNetworkAccessManager>
#include <QFuture>
#include <QStringBuilder>
#include <QNetworkReply>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>

#include "qweblatejsonhelpers_p.h"
#include "qweblatefuturehelpers_p.h"

class QWeblateClientPrivate {
public:
    QNetworkAccessManager client;
    QUrl instanceUrl;
    QString token;

    QFuture<QByteArray> authorizedGetRequest(const QString &route) {
        QUrl url = instanceUrl;
        url.setPath(route);

        QNetworkRequest request;
        request.setUrl(url);
        request.setRawHeader(QByteArrayLiteral("Authorization"), (u"Token " % token).toUtf8());

        auto *reply = client.get(request);

        auto futureInterface = std::make_shared<QFutureInterface<QByteArray>>();
        QObject::connect(reply, &QNetworkReply::finished, reply, [=] {
            futureInterface->reportResult(reply->readAll());
            futureInterface->reportFinished();
        });

        return futureInterface->future();
    };

    template <typename T, typename ConversionFunction>
    QFuture<T> authorizedJsonGetRequest(const QString &route, ConversionFunction fun) {
        return map_future<QByteArray, T>(authorizedGetRequest(route), [=](const auto &&data) {
            auto doc = QJsonDocument::fromJson(data);
            return fun(std::move(doc));
        });
    }

    template <typename T>
    QFuture<QVector<T>> getArray(const QString &route) {
        return authorizedJsonGetRequest<QVector<T>>(route, [this](const QJsonDocument &&doc) {
            return fromJsonArray<T>(doc.array(), this);
        });
    }

    template <typename T>
    QFuture<T> getObject(const QString &route) {
        return authorizedJsonGetRequest<T>(route, [this](const QJsonDocument &&doc) {
            return T(doc.object(), this);
        });
    }
};
