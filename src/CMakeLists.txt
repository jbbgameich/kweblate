add_executable(kweblate main.cpp resources.qrc)
target_link_libraries(kweblate
    Qt5::Core
    Qt5::Gui
    Qt5::Qml
    Qt5::Quick
    Qt5::QuickControls2
    Qt5::Svg
    KF5::I18n
    qweblateclient)
install(TARGETS kweblate ${KDE_INSTALL_TARGETS_DEFAULT_ARGS})
