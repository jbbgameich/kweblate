#include <QApplication>
#include <QQmlApplicationEngine>
#include <QtQml>
#include <QUrl>
#include <KLocalizedContext>

#include <qweblateclient.h>
#include <qweblateendpoints_p.h>
#include <qweblateuser.h>
#include <qweblateresults.h>
#include <qweblategroup.h>
#include <qweblatefuturehelpers_p.h>
#include <qweblatetranslation.h>
#include <qweblateproject.h>
#include <qweblaterepository.h>
#include <qweblatecomponent.h>
#include <qweblatelanguage.h>

Q_DECL_EXPORT int main(int argc, char *argv[])
{
    QGuiApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QApplication app(argc, argv);
    QCoreApplication::setOrganizationName("KDE");
    QCoreApplication::setOrganizationDomain("kde.org");
    QCoreApplication::setApplicationName("kweblate");

    QQmlApplicationEngine engine;

    QWeblateClient client(QStringLiteral("http://localhost"), QStringLiteral("diiGYWHvhPKxyvO4GmZcRYLq25UsTwi5IOeSpmZz"));
    finished_then(client.endpoints(), [](const auto &&result) {
        qDebug() << result.projects();
    });

    finished_then(client.user(QStringLiteral("admin")), [](const auto &&result) {
        qDebug() << result.isSuperuser();
    });

    finished_then(client.users(), [](const auto &&result) {
        qDebug() << result.results().size();
    });

    finished_then(client.groups(), [&](const auto &&result) {
        qDebug() << result.results().first().name();
    });

    finished_then(client.group(3), [](const auto &&result) {
        qDebug() << "group" << result.name();
    });

    finished_then(client.projects(), [](const auto &&result) {
        qDebug() << result.results().first().name();
    });

    finished_then(client.projectRepository(QStringLiteral("openboard")), [](const auto &&result) {
        qDebug() << result.needsMerge();
    });

    finished_then(client.projectComponents(QStringLiteral("openboard")), [](const auto &&result) {
        qDebug() << "number of components" << result.results().size();
    });

    finished_then(client.translations(), [](const QWeblateResults<QWeblateTranslation> &&result) {
        if (auto optional = result.next()) {
            finished_then(std::move(optional.value()), [](const auto &&result) {
                qDebug() << result.results().first().fileName();
            });
        }
    });

    finished_then(client.translation(QStringLiteral("openboard"),
                                     QStringLiteral("translations"),
                                     QStringLiteral("ru")),
                  [](const QWeblateTranslation &&result) {
        qDebug() << result.fileName();
    });

    finished_then(client.translationsFetchFile("openboard", "translations", "ru"), [](const auto &&result) {
        //qDebug() << QString::fromUtf8(result);
    });

    finished_then(client.projectLanguages(QStringLiteral("openboard")), [&](const QVector<QWeblateLanguage> &&result) {
        for (const auto &lang : result) {
            qDebug() << lang.language();
        }
    });

    engine.rootContext()->setContextObject(new KLocalizedContext(&engine));
    engine.load(QUrl(QStringLiteral("qrc:///main.qml")));

    if (engine.rootObjects().isEmpty()) {
        return -1;
    }

    return app.exec();
}
