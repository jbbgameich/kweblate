# KWeblate

An attempt to build some reusable components to get the KDE translation system situation unstuck.

## Basic Plan

- [ ] Create QWeblateClient
- [ ] Create a client app based on it
- [ ] Possibly other integrations, KIO workers, etc

- [ ] Hopefully make translators happy enough.
